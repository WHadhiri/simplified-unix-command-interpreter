#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>

void trim(char *str);

int main(int argc, char *argv[])
{
    int out, i, in;
    char *programName, *fileName, *fileE, *cmd, *arg;
    char command[1024], cmd1[50], cmd2[50];

    for (i = 1; i < argc; i++)
    {
        strcat(command, argv[i]);
        strcat(command, " ");
    }
    if (strstr(command, ">") == NULL || strstr(argv[1], ">") != NULL || strstr(argv[i - 1], ">") != NULL)
    {
        fprintf(stderr, "Format '>': ./sup commande '>' fichier\n");
        exit(EXIT_FAILURE);
    }
    else
    {
        cmd = strtok(command, ">");
        strcpy(cmd1, cmd);
        while (cmd != NULL)
        {
            strcpy(cmd2, cmd);
            cmd = strtok(NULL, ">");
        }
        trim(cmd1);
        trim(cmd2);
        fileName = cmd2;
        out = open(fileName, O_WRONLY | O_TRUNC | O_CREAT, 0666);
        dup2(out, 1);
        system(cmd1);
        close(out);
        return 0;
    }
}

void trim(char *str)
{
    int index, i;

    index = 0;
    while (str[index] == ' ' || str[index] == '\t' || str[index] == '\n')
    {
        index++;
    }

    i = 0;
    while (str[i + index] != '\0')
    {
        str[i] = str[i + index];
        i++;
    }
    str[i] = '\0';

    i = 0;
    index = -1;
    while (str[i] != '\0')
    {
        if (str[i] != ' ' && str[i] != '\t' && str[i] != '\n')
        {
            index = i;
        }

        i++;
    }
    str[index + 1] = '\0';
}