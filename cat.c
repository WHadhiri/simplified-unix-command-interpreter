#include<stdlib.h>
#include<unistd.h>
#include<stdio.h>
#include <sys/types.h>
#include <sys/stat.h>

int isDir(const char* target)
{
   struct stat statbuf;
   if(stat(target, &statbuf) !=0)
	return 0;
   return S_ISDIR(statbuf.st_mode);
}

int main (int argc, char **argv){
if(argc != 2){
 fprintf(stderr, "Format ouvrir: ./ouvrir File \n");
 exit(EXIT_FAILURE);
}else{
if(isDir(argv[1]) == 0){
if(access(argv[1], F_OK) != -1){ 
FILE *f=fopen(argv[1],"r");
int lettre=0;
while((lettre = fgetc(f)) !=EOF)
{
printf("%c", lettre);
}
fclose(f);
}else{
 fprintf(stderr, "fichier introuvable \n");
 exit(EXIT_FAILURE);
}
}else{
fprintf(stderr, "%s est un dossier\n", argv[1]);
 exit(EXIT_FAILURE);	
}
}
return 0;

}
