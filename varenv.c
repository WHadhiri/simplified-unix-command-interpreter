#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

int main(int argc, char *argv[], char *envp[])
{
    int i;
    if (argc == 1)
    {
        for (i = 0; envp[i]!= NULL; i++)
        {
            printf("%s\n", envp[i]);
        }
        
    }else{
        int j;
        for (j = 1; j < argc ; j++)
        {
            char *arg = argv[j];
            printf("%s = ", argv[j]);
            printf("%s\n", getenv(arg));
        }
        
    }
    return 0;
}
