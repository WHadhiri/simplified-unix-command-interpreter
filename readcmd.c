#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>

int main(int argc, char *argv[])
{
    int i, tot = 0;
    char cmds[10][50];

    if (argc != 2)
    {
        fprintf(stderr, "Format : ./readfile fichier\n");
        exit(EXIT_FAILURE);
    }
    else
    {
        FILE *f = fopen(argv[1], "r");
        if (f == NULL)
        {
            fprintf(stderr, "%s File not Found\n", argv[1]);
            exit(EXIT_FAILURE);
        }
        else
        {
            while (fgets(cmds[i], 50, f))
            {
                cmds[i][strlen(cmds[i])] = '\0';
                i++;
            }
            tot = i;
            for (i = 0; i < tot; ++i)
            {
                printf("*** Running Command < %s > ***\n", cmds[i]);
                system(cmds[i]);
            }
        }
    }
    return 0;
}