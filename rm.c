#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
int main (int argc, char **argv){
if(argc != 2){
 fprintf(stderr, "Format delete: ./delete File \n");
 exit(EXIT_FAILURE);
}else{
if(access(argv[1], F_OK) != -1){ 
remove(argv[1]);
 fprintf(stdout, "fichier supprimée avec succès \n");
}else{
 fprintf(stderr, "fichier introuvable \n");
 exit(EXIT_FAILURE);
}
}
return 0;

}
