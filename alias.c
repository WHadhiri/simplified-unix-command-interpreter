#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>

int main(int argc, char **argv)
{
    //char static *path = "./.aliases";
    char s[50];
    if (argc != 3)
    {
        fprintf(stderr, "Format shcut: ./shcut name 'commande' ('' are important) \n");
        exit(EXIT_FAILURE);
    }
    else
    {
        FILE *f = fopen("/home/ooredoo/.aliases", "a+");
        char alias[1024];
        strcpy(alias, "alias ");
        strcat(alias, argv[1]);
        strcat(alias, "='");
        strcat(alias, argv[2]);
        strcat(alias, "'\n");
        printf("%s is added to .aliases\n", argv[1]);
        //system("shopt -s expand_aliases");
        fputs(alias, f);
        //system("source /home/ooredoo/.bashrc");
        fclose(f);
    }
    return 0;
}
