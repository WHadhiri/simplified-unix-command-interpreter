#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
 
int main(int argc, char* argv[])
{
	int srcFD,destFD,nbread,nbwrite;
	char *buff[1024];
	if(argc != 3)
	{
		printf("\nUsage: copier fichier_origine fichier_destination\n");
		exit(EXIT_FAILURE);
	}
	srcFD = open(argv[1],O_RDONLY);
	if(srcFD == -1)
	{
		printf("\nError opening file %s \n", argv[1]);
		exit(EXIT_FAILURE);	
	}
	destFD = open(argv[2],O_WRONLY | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);
	if(destFD == -1)
	{
		printf("\nError opening file %s \n", argv[2]);
		exit(EXIT_FAILURE);
	}
	while((nbread = read(srcFD,buff,1024)) > 0)
	{
		if(write(destFD,buff,nbread) != nbread)
			printf("\nError in writing data to %s\n",argv[2]);
	}
	close(srcFD);
	close(destFD);
	return 0;
}
