#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <errno.h>
int main(int argc, char **argv)
{
	char s[512];
	if (argc != 2)
	{
		fprintf(stderr, "Expression entrer incorrect : entrer <Chemin/Dossier>\n");
		exit(EXIT_FAILURE);
	}
	else
	{
		printf("Chemin Précédent : %s\n", getcwd(s, 512));
		errno = chdir(argv[1]);
		if (errno != 0)
		{
			printf("Error changing directory\n");
			exit(EXIT_FAILURE);
		}
		printf("Chemin Courant : %s\n", getcwd(s, 512));
	}
	return 0;
}
