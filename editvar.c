#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
    int i;
    char newpath[50];
    if (argc < 3)
    {
        printf("Format editvar: ./editvar VAR str\n");
    }
    else
    {
        char *arg1 = argv[1];
        char *var = getenv(arg1);
        char *str = argv[2];
        sprintf(newpath, "%s=%s:%s", arg1, var, str);
        putenv(newpath);
        printf("%s\n", getenv(arg1));
    }
    return 0;
}
