#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

/*Définition de pcb.*/
typedef struct pcb
{
    int nomProcessus;
    int tempsArriver;
    int tempsExecution;
    int saveTempsExecution;
    int w_time;
    int t_time;

} pcb;

/*Fonctions de manupilation des processus*/
void ajouterProcessus(pcb p[], int);
void triProcessus(pcb p[], int);
void triProcessusSjf(pcb p[], int);
void afficherProcessus(pcb p[], int);
void fifoProcessus(pcb p[], int);
void rrProcessus(pcb p[], int, int);
void sjfProcessus(pcb p[], int);

/*Debut main*/
int main(void)
{

    printf("                *----------------------------*\n");
    printf("                *                            *\n");
    printf("                *       ORDONNANCEUR         *\n");
    printf("                *     FIFO && SJF && RR      *\n");
    printf("                *                            *\n");
    printf("                *----------------------------*\n\n");

    pcb p[100];
    char nMenu;

    int n, quantum;
    n = nbProcessus(p);
    ajouterProcessus(p, n);
x:
    printf("*--------------------------*\n");
    printf("*                          *\n");
    printf("*  Menu :                  *\n");
    printf("*                          *\n");
    printf("*    1 Affichage prc       *\n");
    printf("*    2 FIFO                *\n");
    printf("*    3 SJF                 *\n");
    printf("*    4 RR                  *\n");
    printf("*    5 Quitter             *\n");
    printf("*                          *\n");
    printf("*--------------------------*\n");
m:
    printf("\n\nEntrez un numero de menu : ");
    scanf("%s", &nMenu);
    switch (nMenu)
    {
    case '1':
        goto a;
        break;
    case '2':
        goto b;
        break;
    case '3':
        goto c;
        break;
    case '4':
        goto d;
        break;
    case '5':
        goto e;
        break;
    default:
        printf("\nVous avez pas introduit un numero de menu...\n\n");
        goto x;
    }

a:
    afficherProcessus(p, n);
    goto m;
b:
    fifoProcessus(p, n);
    goto m;
c:
    sjfProcessus(p, n);
    goto m;
d:
    printf("Entrez le quantum : ");
    scanf("%d", &quantum);
    rrProcessus(p, quantum, n);
    goto m;
e:
    system("clear && cd .. && ./miniProjet");
}

/* Fonction pour definir le nombre de processus*/
int nbProcessus(pcb p[])
{
    int nb = 0;
    char text[10][50];
    FILE *f = fopen("processList", "r");
    //p = (pcb *)malloc(sizeof(pcb));
    while (fgets(text[nb], 50, f) != NULL)
    {
        text[nb][strlen(text[nb])] = '\0';
        nb++;
    }
    return nb;
}

/*Fonction pour ajouter les processus arrivent à la file d'attente des processus prêts*/
void ajouterProcessus(pcb p[], int n)
{
    pcb ps;
    char *token;
    char text[10][50];
    int i = 0, j, nomProcessus, tA, tE;

    FILE *f = fopen("processList", "r");
    while (fgets(text[i], 50, f) != NULL)
    {
        text[i][strlen(text[i])] = '\0';
        i++;
    }

    for (j = 0; j < n; j++)
    {
        token = strtok(text[j], ",");
        nomProcessus = atoi(token);
        token = strtok(NULL, ",");
        tA = atoi(token);
        while ((token = strtok(NULL, ",")) != NULL)
        {
            tE = atoi(token);
        }
        p[j].nomProcessus = nomProcessus;
        p[j].tempsArriver = tA;
        p[j].tempsExecution = tE;
        p[j].saveTempsExecution = p[j].tempsExecution;
        p[j].w_time = 0;
        p[j].t_time = 0;
    }
}
/*Fonction pour trier les processus selon le temps d'arrivé*/
void triProcessus(pcb p[], int n)
{
    int i, j;
    pcb temp;
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n - 1 - i; j++)
        {
            if (p[j].tempsArriver > p[j + 1].tempsArriver)
            {
                temp = p[j];
                p[j] = p[j + 1];
                p[j + 1] = temp;
            }
        }
    }
}

/*Fonction pour trier les processus selon le temps d'execution*/
void triProcessusSjf(pcb p[], int n)
{
    int i, j;
    pcb temp;
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n - 1 - i; j++)
        {
            if (p[j].tempsArriver > p[j + 1].tempsArriver)
            {
                temp = p[j];
                p[j] = p[j + 1];
                p[j + 1] = temp;
            }
            else if (p[j].tempsArriver == p[j + 1].tempsArriver)
            {
                if (p[j].tempsExecution > p[j + 1].tempsExecution)
                {
                    temp = p[j];
                    p[j] = p[j + 1];
                    p[j + 1] = temp;
                }
            }
        }
    }
}

/*Fontion pour afficher les processus de la file d'attente prêt*/
void afficherProcessus(pcb p[], int n)
{
    int i, j;
    // print top bar
    printf(" ");
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < p[i].tempsExecution; j++)
            printf("--");
        printf(" ");
    }
    printf("\n|");

    // printing process name in the middle
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < p[i].tempsExecution - 1; j++)
            printf(" ");
        printf("P%d", p[i].nomProcessus);
        for (j = 0; j < p[i].tempsExecution - 1; j++)
            printf(" ");
        printf("|");
    }
    printf("\n ");
    // printing bottom bar
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < p[i].tempsExecution; j++)
            printf("--");
        printf(" ");
    }
    printf("\n");

    // printing the time line
    printf("0");
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < p[i].tempsExecution; j++)
            printf("  ");
        if (p[i].t_time > 9)
            printf("\b"); // backspace : remove 1 space
        printf("%d", p[i].t_time + p[i].tempsArriver);
    }
    printf("\n");
}

/*Fonction qui fait l'allocation de processeur selon la politique FIFO*/
void fifoProcessus(pcb p[], int n)
{

    pcb q[n];
    int i, nbProcessus = n;
    int sum_waiting_time = 0, sum_turnaround_time = 0;
    int service_time[n];
    service_time[0] = 0;
    for (i = 0; i < n; i++)
    {
        q[i] = p[i];
    }
    triProcessus(q, n);
    q[0].t_time = q[0].tempsExecution;
    for (i = 1; i < n; i++)
    {
        service_time[i] = service_time[i - 1] + q[i - 1].tempsExecution;
        q[i].w_time = service_time[i] - q[i].tempsArriver;

        if (q[i].w_time < 0)
        {
            q[i].w_time = 0;
        }
        sum_waiting_time += q[i].w_time;
    }
    for (i = 0; i < n; i++)
    {
        q[i].t_time = q[i].w_time + q[i].tempsExecution;
        sum_turnaround_time += q[i].t_time;
    }
    printf("***********************FIFO***********************\n");
    afficherProcessus(q, n);

    printf("\n\n");
    printf("Temps moyen de traitement est : %-2.2lf\n", (double)sum_turnaround_time / (double)n);
    printf("Temps moyen d'attente est : %-2.2lf\n", (double)sum_waiting_time / (double)n);
}

/*Fonction qui fait l'allocation de processeur selon la politique RR*/
void rrProcessus(pcb p[], int quantum, int n)
{
    int i, total = 0, x = n, counter = 0;
    int sum_waiting_time = 0, sum_turnaround_time = 0;
    int temp[n], temp2[n];

    triProcessus(p, n);

    for (i = 0; i < n; i++)
    {
        temp[i] = p[i].tempsExecution;
        temp2[i] = p[i].tempsExecution;
    }

    for (total = 0, i = 0; x != 0;)
    {
        if (temp[i] <= quantum && temp[i] > 0)
        {
            total = total + temp[i];
            temp[i] = 0;
            counter = 1;
        }
        else if (temp[i] > 0)
        {
            temp[i] = temp[i] - quantum;
            total = total + quantum;
        }
        if (temp[i] == 0 && counter == 1)
        {
            x--;
            p[i].w_time = p[i].w_time + total - p[i].tempsArriver - p[i].tempsExecution;
            p[i].t_time = p[i].t_time + total - p[i].tempsArriver;
            sum_waiting_time += p[i].w_time;
            sum_turnaround_time += p[i].t_time;
            counter = 0;
        }
        if (i == n - 1)
        {
            i = 0;
        }
        else if (p[i + 1].tempsArriver <= total)
        {
            i++;
        }
        else
        {
            i = 0;
        }
        p[i].w_time = 0;
        p[i].t_time = 0;
    }

    int j = 0;
    int bt[n];
    printf("\n***************************************RR(Q=%d)***************************************\n", quantum);
    for (i = p[0].tempsArriver; i < total;)
    {
        bt[j] = p[j].tempsExecution;
        if (bt[j] >= quantum)
        {
            printf("P%d\t", p[j].nomProcessus);
            i += quantum;
            bt[j] = bt[j] - quantum;
        }
        else if (p[j].tempsExecution > 0)
        {
            printf("P%d\t", p[j].nomProcessus);
            i += bt[j];
            bt[j] = 0;
        }
        j++;
        if (j >= n)
        {
            j = 0;
        }
    }
    printf("\n");

    j = 0;
    for (i = p[0].tempsArriver; i < total;)
    {
        if (temp2[j] >= quantum)
        {
            printf("%d\t", i + quantum);
            i += quantum;
            temp2[j] = temp2[j] - quantum;
        }
        else if (temp2[j] > 0)
        {
            printf("%d\t", i + temp2[j]);
            i += temp2[j];
            temp2[j] = 0;
        }
        j++;
        if (j >= n)
        {
            j = 0;
        }
    }
    printf("\n\n");
    printf("Temps moyen de traitement est : %-2.2lf\n", (double)sum_turnaround_time / (double)n);
    printf("Temps moyen d'attente est : %-2.2lf\n", (double)sum_waiting_time / (double)n);
}

void sjfProcessus(pcb p[], int n)
{
    pcb q[n];
    int i, nbProcessus = n;
    int sum_waiting_time = 0, sum_turnaround_time = 0;
    int service_time[n];
    service_time[0] = 0;
    for (i = 0; i < n; i++)
    {
        q[i] = p[i];
    }
    triProcessusSjf(q, n);
    q[0].t_time = q[0].tempsExecution;
    for (i = 1; i < n; i++)
    {
        service_time[i] = service_time[i - 1] + q[i - 1].tempsExecution;
        q[i].w_time = service_time[i] - q[i].tempsArriver;

        if (q[i].w_time < 0)
        {
            q[i].w_time = 0;
        }
        sum_waiting_time += q[i].w_time;
    }
    for (i = 0; i < n; i++)
    {
        q[i].t_time = q[i].w_time + q[i].tempsExecution;
        sum_turnaround_time += q[i].t_time;
    }
    printf("***********************SJF***********************\n");
    afficherProcessus(q, n);

    printf("\n\n");
    printf("Temps moyen de traitement est : %-2.2lf\n", (double)sum_turnaround_time / (double)n);
    printf("Temps moyen d'attente est : %-2.2lf\n", (double)sum_waiting_time / (double)n);
}