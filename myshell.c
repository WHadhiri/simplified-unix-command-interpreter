#include <sys/wait.h>
#include <stdio.h>
#include <sys/types.h>
#include <pwd.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <string.h>
#include <sys/mman.h>
#include <errno.h>
void DisplayPrompt()
{

    char buf[50];
    char *ptr;
    ptr = getcwd(buf, 50);

    struct passwd *getpwuid(uid_t uid);
    struct passwd *p;
    uid_t uid = 0;
    if ((p = getpwuid(uid)) == NULL)
        perror("getpwuid() error");
    else
    {
        printf("[");
        printf("\033[0;31m");
        printf("%s", p->pw_name);
        printf("\033[0m");
        printf("@");
        printf("\033[0;31m");
        printf("%s", ptr);
        printf("\033[0m");
        printf("]>");
    }
}

void showhelp()
{
    printf("\033[0;31m");
    printf("************************************************************************\n");
    printf("*                                                                      *\n");
    printf("*   \tCommand                   \tAction                         *\n");
    printf("*                                                                      *\n");
    printf("************************************************************************\n");
    printf("*                                                                      *\n");
    printf("*\t1 - access                   \t(Change Directory)             *\n");
    printf("*\t2 - ouvrir                   \t(Read File Content)            *\n");
    printf("*\t3 - dir                      \t(Display Directory's Content)  *\n");
    printf("*\t4 - emplacement            \t(Display Current Path)         *\n");
    printf("*\t5 - copier            \t(Copy File Content To Other File )     *\n");
    printf("*\t6 - delete                   \t(Delete File)                  *\n");
    printf("*\t7 - editvar                  \t(Edit Environment Variable)    *\n");
    printf("*\t8 - inf            \t(Redirect Standard Output In A File)   *\n");
    printf("*\t9 - pipe       \t(Redirect Standard Output To Standard Input)   *\n");
    printf("*\t10 - readfile          \t(Read & Execute Commands From A File)  *\n");
    printf("*\t11 - shcut            \t(Create An Alias Of A Command)         *\n");
    printf("*\t12 - sup            \t(Redirect Standard Input From A File)  *\n");
    printf("*\t13 - varenv                  \t(Read Environment Variables)   *\n");
    printf("*\t14 - done                  \t(Exit Shell)                   *\n");
    printf("*                                                                      *\n");
    printf("*                                                                      *\n");
    printf("************************************************************************\n");
    printf("\033[0m");
}

int cmdalias(char *alias)
{
    FILE *f = fopen("/home/ooredoo/.aliases", "r");
    int ex = 0;
    char texte[265];
    while (fgets(texte, 255, f) != NULL)
    {
        if (strstr(texte, alias) != NULL)
        {
            ex++;
        }
    }
    fclose(f);
    return ex;
}

void trim(char *str);

int main(int argc, char *argv[])
{

    chdir(getenv("HOME"));
    if (access("profile", F_OK) == -1)
    {
        FILE *f = fopen("profile", "a+");
        char env1[100];
        char env2[100];
        strcpy(env1, "PATH=");
        strcat(env1, getenv("PATH"));
        strcat(env1, "\n");
        fputs(env1, f);
        strcpy(env2, "HOME=");
        strcat(env2, getenv("HOME"));
        strcat(env2, "\n");
        fputs(env2, f);
        fclose(f);
    }
    else
    {
        FILE *f = fopen("profile", "r");
        char text[256];
        int exist = 0;
        while (fgets(text, 255, f) != NULL)
        {
            if (strstr(text, "PATH") == 0)
                exist++;
            if (strstr(text, "HOME") == 0)
                exist++;
        }
        if (exist != 2)
        {
            fprintf(stderr, "PATH/HOME Variables not found in Profile file\n");
            exit(EXIT_FAILURE);
        }
        fclose(f);
    }
    char cmd[510];
    system("cat ./Bureau/ProjetSE/banner");
    DisplayPrompt();
    while (strcmp(cmd, "done") != 0)
    {
        if (fgets(cmd, 510, stdin) == NULL)
            printf("\n");
        trim(cmd);
        char exect[50], var2[50];
        char *token;
        strcpy(exect, cmd);
        strcat(exect, "=");
        trim(exect);
        if (cmdalias(exect) == 1)
        {
            const char v[2] = "=";
            FILE *f = fopen("/home/ooredoo/.aliases", "r");
            char texte[265];
            while (fgets(texte, 255, f) != NULL)
            {
                if (strstr(texte, exect))
                {
                    token = strtok(texte, v);
                    while (token != NULL)
                    {
                        strcpy(var2, token);
                        token = strtok(NULL, v);
                    }
                }
            }
            const char p[2] = "'";
            char ex[50];
            token = strtok(var2, p);
            strcpy(ex, token);
            fclose(f);
            system(ex);
            DisplayPrompt();
        }
        else if (strcmp(cmd, "done") == 0)
        {
            system("clear && miniProjet");
        }
        else if (strstr(cmd, "access") != NULL)
        {
            char *token;
            char arg[50];
            token = strtok(cmd, " ");
            strcpy(arg, cmd);
            while (token != NULL)
            {
                strcpy(arg, token);
                token = strtok(NULL, " ");
            }
            errno = chdir(arg);
            if (errno != 0)
            {
                printf("Error changing directory\n");
                DisplayPrompt();
            }
            else
            {
                DisplayPrompt();
            }
        }
        else if (strcmp(cmd, "help") == 0)
        {
            showhelp();
            DisplayPrompt();
        }
        else
        {
            system(cmd);
            DisplayPrompt();
        }
    }
    return EXIT_SUCCESS;
}

void trim(char *str)
{
    int index, i;

    index = 0;
    while (str[index] == ' ' || str[index] == '\t' || str[index] == '\n')
    {
        index++;
    }

    i = 0;
    while (str[i + index] != '\0')
    {
        str[i] = str[i + index];
        i++;
    }
    str[i] = '\0';

    i = 0;
    index = -1;
    while (str[i] != '\0')
    {
        if (str[i] != ' ' && str[i] != '\t' && str[i] != '\n')
        {
            index = i;
        }

        i++;
    }
    str[index + 1] = '\0';
}